package expert.sys.api.service;

import org.springframework.stereotype.Service;

import expert.sys.api.entity.Question;
import expert.sys.api.repository.ExpertSystemRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ExpertSystemService {
    private final ExpertSystemRepository repository;

    public void post(Question question) {
        repository.save(question);
    }

    public Question get(Integer id) {
        return (repository.existsById(id)) ? repository.findById(id).get() : null;
    }

}
