package expert.sys.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import expert.sys.api.entity.Question;

@Repository
public interface ExpertSystemRepository extends JpaRepository<Question, Integer> {
}
