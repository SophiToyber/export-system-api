package expert.sys.api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import expert.sys.api.entity.Question;
import expert.sys.api.service.ExpertSystemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/question")
public class ExpertSystemController {

    private final ExpertSystemService expertService;

    @PostMapping
    public void create(@RequestBody Question question) {
        log.info("Request to question: {}", question.toString());
        expertService.post(question);
    }

    @GetMapping("/{id}")
    public Question get(@PathVariable Integer id) {
        log.info("To get request: {}", id);
        return expertService.get(id);
    }
}
