FROM openjdk:11

COPY build/libs/*.jar expert-sys-api.jar

EXPOSE 8184 8185

ENTRYPOINT [ "java", "-jar", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8185", "expert-sys-api.jar"]
